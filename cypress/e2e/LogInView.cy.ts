describe('LogInPage Component', () => {
  beforeEach(() => {
    cy.visit('/LogIn')

  })

  it('should display the login form by default', () => {
    cy.get('#LogInView').should('be.visible')
    cy.get('#CreateUserView').should('not.be.visible')
  })

  it('should display error messages for invalid input when creating a new user', () => {
    cy.get('#SwitchToCreateUserButton').click()

    cy.get('#usernameInput').type('    ') // Empty name
    cy.get('#newEmailInput').type('invalidemail') // Invalid email
    cy.get('#newPasswordInput').type('123') // Short password
    cy.get('#ConfirmPasswordInput').type('password123') // Mismatched confirm password

    cy.get('#ConfirmPasswordInput').blur()

    cy.get('[data-testid="UsernameError"]').should('be.visible')
    cy.get('[data-testid="EmailError"]').should('be.visible')
    cy.get('[data-testid="PasswordError"]').should('be.visible')
    cy.get('[data-testid="PasswordConfirmError"]').should('be.visible')
  })

})
