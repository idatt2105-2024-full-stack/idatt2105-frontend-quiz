
describe('HomeView', () => {
  it('navigates to /LogIn when Get Started button is clicked', () => {
    cy.visit('/');

    cy.get('#GetStartedBtn').click();

    cy.url().should('include', '/LogIn');
  });

  it('navigates to /MakeQuiz when Create Quiz button is clicked', () =>{
    cy.visit('/');

    cy.get('[data-testid="MakeQuizBtn"]').click();

    cy.url().should('include', '/MakeQuiz');
  })

  it('navigates to /BrowseQuizes when Browse Quizzes button is clicked', () =>{
    cy.visit('/');

    cy.get('[data-testid="BrowseQuizzesBtn"]').click();

    cy.url().should('include', '/BrowseQuizes');
  })

  it('navigates to /BrowseMyQuizes when My Quizzes button is clicked', () =>{
    cy.visit('/');

    cy.get('[data-testid="MyQuizzesBtn"]').click();

    cy.url().should('include', '/BrowseMyQuizes');
  })

});