describe('Quiz Info Component', () => {
  beforeEach(() => {
    // Intercept login request and return success response
    cy.intercept('POST', 'http://localhost:8080/api/auth/login', {
      statusCode: 200,
      body: {
        token: 'mocked_jwt_token'
      }
    }).as('logIn')

    cy.visit('/MakeQuiz')
  })

  it('should render input fields and labels correctly', () => {
    // Verify that all input fields and labels are visible
    cy.get('#QuizNameDiv').should('be.visible')
    cy.get('#QuizDescriptionDiv').should('be.visible')
    cy.get('#QuizTagsDiv').should('be.visible')
    cy.get('#QuizRatingDiv').should('be.visible')

    // Verify that labels have correct text
    cy.get('#QuizNameLabel').should('have.text', 'Quiz Name')
    cy.get('#QuizDescriptionLabel').should('have.text', 'Description')
    cy.get('#QuizTagsLabel').should('have.text', 'Quiz Tag')
    cy.get('#QuizRatingLabel').should('have.text', 'Difficulty')
  })
})
