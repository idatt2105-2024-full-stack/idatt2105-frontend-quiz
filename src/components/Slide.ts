export interface Slide {

  question: string;
  answers: string[];
  correctAnswers: boolean[];
  activeAnswers: boolean[];

}