import { mount } from '@vue/test-utils'
import HomeButtons from '../HomeComponents/HomeButtons.vue'
import { describe, it, expect, beforeEach} from 'vitest'
import { createPinia } from 'pinia'


describe('HomeButtons Component', () => {
  let pinia = createPinia()
  let wrapper = mount(HomeButtons, {
    global:{
      plugins:[pinia]
    }
  })

  beforeEach(function(){
    pinia = createPinia()
    wrapper = mount(HomeButtons, {
      global:{
        plugins:[pinia]
      }
    })
  })

  it('renders the Create Quiz button', () => {
    // Find the Create Quiz button by data-testid
    const createQuizBtn = wrapper.find('[data-testid="MakeQuizBtn"]')

    // Assert that the Create Quiz button exists
    expect(createQuizBtn.exists()).toBe(true)
  })

  it('renders the Browse Quizzes button', () => {
    // Find the Browse Quizzes button by data-testid
    const browseQuizzesBtn = wrapper.find('[data-testid="BrowseQuizzesBtn"]')

    // Assert that the Browse Quizzes button exists
    expect(browseQuizzesBtn.exists()).toBe(true)
  })

  it('renders the My Quizzes button', () => {
    // Find the My Quizzes button by data-testid
    const myQuizzesBtn = wrapper.find('[data-testid="MyQuizzesBtn"]')

    // Assert that the My Quizzes button exists
    expect(myQuizzesBtn.exists()).toBe(true)
  })
})
