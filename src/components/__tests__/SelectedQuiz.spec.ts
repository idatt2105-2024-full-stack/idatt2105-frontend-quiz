import { mount } from '@vue/test-utils'
import SelectedQuiz from '../SelectedQuizComponents/SelectedQuiz.vue'
import { describe, it, expect, beforeEach} from 'vitest'
import { createPinia } from 'pinia'


describe('SelectedQuiz Component', () => {

  let pinia = createPinia()
  let wrapper = mount(SelectedQuiz, {
    global:{
      plugins:[pinia]
    }
  })

  beforeEach(function(){
    pinia = createPinia()
    wrapper = mount(SelectedQuiz, {
      global:{
        plugins:[pinia]
      }
    })
  })


  it('renders title and description elements', () => {
    // Check if the title and description elements exist
    expect(wrapper.find('#QuizTitle').exists()).toBe(true)
    expect(wrapper.find('#Description').exists()).toBe(true)
  })

  it('renders "Take quiz" button', () => {
    // Check if the "Take quiz" and "Delete quiz" buttons exist
    expect(wrapper.find('#TakeQuiz').exists()).toBe(true)
  })

  it('contains thumbnail image and button row', () => {
    // Check if the thumbnail image and button row container exist
    expect(wrapper.find('#Thumbnail').exists()).toBe(true)
    expect(wrapper.find('#ButtonRow').exists()).toBe(true)
  })

  it('contains selected quiz view container', () => {
    // Check if the selected quiz view container exists
    expect(wrapper.find('#SelectedQuizView').exists()).toBe(true)
  })
})
