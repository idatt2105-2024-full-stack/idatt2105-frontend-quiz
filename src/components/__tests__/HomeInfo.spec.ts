import { mount } from '@vue/test-utils'
import { describe, it, expect, beforeEach} from 'vitest'
import HomeInfo from '../HomeComponents/HomeInfo.vue'
import { createPinia } from 'pinia'

describe('HomeInfo Component', () => {
    let pinia = createPinia()
    let wrapper = mount(HomeInfo, {
      global:{
        plugins:[pinia]
      }
    })

    beforeEach(function(){
      pinia = createPinia()
      wrapper = mount(HomeInfo, {
        global:{
          plugins:[pinia]
        }
      })
    })

  it('renders the Welcome text', () => {
    // Find the Welcome text element
    const welcomeText = wrapper.find('#WelcomeText')

    // Assert that the Welcome text exists
    expect(welcomeText.exists()).toBe(true)
  })

  it('renders the Get Started button', () => {
    // Find the Get Started button
    const getStartedBtn = wrapper.find('#GetStartedBtn')

    // Assert that the Get Started button exists
    expect(getStartedBtn.exists()).toBe(true)
  })
})
