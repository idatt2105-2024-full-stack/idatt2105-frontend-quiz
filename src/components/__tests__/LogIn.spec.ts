import { describe, it, expect, beforeEach} from 'vitest'

import { mount } from '@vue/test-utils'
import LogIn from '../LogInComponents/LogIn.vue'
import { createPinia } from 'pinia'

describe('LogIn', () =>{

  let pinia = createPinia()
  let wrapper = mount(LogIn, {
    global:{
      plugins:[pinia]
    }
  })

  beforeEach(function(){
    pinia = createPinia()
    wrapper = mount(LogIn, {
      global:{
        plugins:[pinia]
      }
    })
  })

  it("Perform New User Name validation", async () =>{
    const nameField = wrapper.find('[data-testid="NewUserName"]')
    await nameField.trigger('blur')
    const nameError = wrapper.find('[data-testid="UsernameError"]').text()
    expect(nameError).toBe("Username is required");
  })

  it("Perform New User Email validation", async () =>{
    const EmailField = wrapper.find('[data-testid="NewUserEmail"]')
    await EmailField.trigger('blur')
    const emailError = wrapper.find('[data-testid="EmailError"]').text()
    expect(emailError).toBe("Please enter a valid E-mail")
  })

  it("Perform New User Password validation", async () =>{
    const passwordField = wrapper.find('[data-testid="NewUserPassword"]')
    await passwordField.trigger('blur')
    const passwordError = wrapper.find('[data-testid="PasswordError"]').text()
    expect(passwordError).toBe("Password must be at least 8 characters long")
  })

  it("Perform New User Password confirmation validation", async () => {
    const passwordField = wrapper.find('[data-testid="NewUserPassword"]')
    await passwordField.setValue("TestPassword")
    await passwordField.trigger('blur')
    const passwordConfirmationField = wrapper.find('[data-testid="NewUserConfirmPassword"]')
    await passwordConfirmationField.setValue("DifferentPassword") // Set different password for confirmation
    await passwordConfirmationField.trigger('blur')
    const passwordConfirmationError = wrapper.find('[data-testid="PasswordConfirmError"]').text() // Extract text content
    expect(passwordConfirmationError).toBe("Passwords do not match")
  })
})
