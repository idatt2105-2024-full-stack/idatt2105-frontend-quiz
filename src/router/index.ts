import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/LogIn',
      name: 'LogIn',
      component: () => import('../views/LogInView.vue')
    },
    {
      path: '/User',
      name: "User",
      component: () => import('../views/UserView.vue')
    },
    {
      path: '/BrowseQuizes',
      name: "BrowseQuizes",
      component: () => import('../views/BrowseQuizView.vue')
    },
    {
      path: '/BrowseMyQuizes',
      name: "BrowseMyQuizes",
      component: () => import('../views/BrowseMyQuizesView.vue')
    },
    {
      path: "/MakeQuiz",
      name: "MakeQuiz",
      component: () => import('../views/MakeQuizView.vue')
    },
    {
      path: "/SelectedQuiz",
      name: "SelectedQuiz",
      component: () => import('../views/SelectedQuizView.vue')
    },
    {
      path: "/TakeQuiz",
      name: "TakeQuiz",
      component: () => import('../views/TakeQuizView.vue')
    }
  ]
})

export default router
