import { ref } from 'vue'
import { defineStore } from 'pinia'

export const BrowseQuizStore = defineStore('Browse', () => {
  const AccessedFrom = ref("Browse Quizzes")

  function setAccessedFrom(accessed : string){
    AccessedFrom.value = accessed
  }

  return { AccessedFrom, setAccessedFrom}
})