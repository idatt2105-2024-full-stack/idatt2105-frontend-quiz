import { ref } from 'vue'
import { defineStore } from 'pinia'

export const selectedQuizStore = defineStore('selectedQuiz', () => {
  const SelectedQuizId = ref(0)
  function setSelectedQuizId(selectedId : number) {
    SelectedQuizId.value = selectedId
  }

  function getSelectedQuizId(){
    return SelectedQuizId.value
  }

  return { SelectedQuizId, setSelectedQuizId, getSelectedQuizId}
})