import { defineStore } from 'pinia';
import { ref } from 'vue';

export const TokenStore = defineStore('Token', () => {
  const token = ref("");

  function getToken(){
    return token.value;
  }

  function setToken(newResult:string){
    token.value = newResult;
  }

  return { token, getToken, setToken };
}, {
  persist:{
    storage: sessionStorage,
  }
});