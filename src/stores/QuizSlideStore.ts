import { defineStore } from 'pinia';
import { ref } from 'vue'
import type { Slide } from '@/components/Slide';

export const SlideStore = defineStore('slide', () => {
  const defaultSlide: Slide = {
    question: 'Click here to write question!',
    answers: ["Click here to write answer option", "Click here to write answer option", "Click here to write answer option", "Click here to write answer option"],
    correctAnswers: [false,false,false,false],
    activeAnswers: [true,true,true,true]
  };

  const slides = ref<Slide[]>([defaultSlide]);

  const QuizName = ref("");
  const QuizDescription = ref("")
  const QuizDifficulty = ref(0)
  const QuizTag = ref(0)

  const reset = () =>{
    slides.value = [defaultSlide]
    QuizName.value = ""
    QuizDescription.value = ""
    QuizDifficulty.value = 0
    QuizTag.value = 0
  }

  const addSlide = (newSlide: Slide) => {
    slides.value.push(newSlide);
  };

  const deleteSlide = (index: number) => {
    slides.value.splice(index, 1);
  };

  const SetQuizName = (name : string) =>{
    QuizName.value = name;
  }

  const GetQuizName = () => {
    return QuizName.value
  }

  const SetQuizDescription = (description : string) => {
    QuizDescription.value = description
  }

  const GetQuizDescription = () =>{
    return QuizDescription.value
  }

  const SetQuizDifficulty = (Difficulty : string) => {
    QuizDifficulty.value = Number(Difficulty)
  }

  const GetQuizDifficulty = () => {
    return QuizDifficulty.value
  }

  const SetQuizTag = (tag : number) => {
    QuizTag.value = tag
  }

  const GetQuizTag = () => {
    return QuizTag.value;
  }

  return {
    slides,
    addSlide,
    deleteSlide,
    QuizName,
    SetQuizName,
    GetQuizName,
    QuizDescription,
    SetQuizDescription,
    GetQuizDescription,
    QuizDifficulty,
    SetQuizDifficulty,
    GetQuizDifficulty,
    QuizTag,
    SetQuizTag,
    GetQuizTag,
    reset
  };

}, {
  // Add the persist option here
  persist: {
    storage: sessionStorage, // data will be stored in sessionStorage
  },
});