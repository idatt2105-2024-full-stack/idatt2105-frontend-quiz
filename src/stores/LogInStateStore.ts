import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useLoginStateStore = defineStore('LogInState', () => {
  const state = ref(false)
  function setState(newState:boolean) {
    state.value = newState;
  }

  function getState(){
    return state.value
  }

  return { state, setState, getState }
}, {
  persist:{
    storage: sessionStorage,
  }
})
