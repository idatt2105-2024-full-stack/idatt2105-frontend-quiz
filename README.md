# Tavern Brawl - Frontend
This project is the frontend part of the Tavern Brawl project.

### Original Contributors
- Sverre Frogner Haugen
- Isak Kallestad Mandal
- Håkon Fredrik Fjellanger

### _NOTE: Development suspended_
This project was a school project for the original contributors. It is not currently being developed further.
#### Tips for further development
Many of the connections to the backend do not handle all possible responses. This is considered by the original teams as first-in-line to be worked once development continues.


## Credentials for manual testing
Given that the backend is running with the test data injected into the database, any tester can use these credentials to login as a ready made user:

Email: sensor@gmail.com

Password: 12345678

## Project Setup

### Recommended IDE
[VSCode](https://code.visualstudio.com/)


### First, to setup the project

```sh
npm install
```

### Compile and Run project

```sh
npm run dev
```

### Build the project ready for deployment

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)
```sh
npm run test:e2e:dev
```

```sh
npm run build
npm run test:e2e
```
